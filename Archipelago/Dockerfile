FROM node:14
LABEL authors=Born-Digital

# docker build -t gulp/app - < gulp.Dockerfile
# docker run -it --mount type=bind,source="$(pwd)",target=/app gulp ./script.sh
# docker rm $(docker ps -a | grep "gulp/app" | awk '{ print $1 }') -f

RUN mkdir -p /app
RUN printf "#!/bin/bash \
\n# This is a bash comment inside the script \
\nchown -R nonroot:nonroot /app/drupal/web/themes/custom \
 \n" > /permissions.sh

RUN printf "#!/bin/bash \
\n# This is a bash comment inside the script \
\necho 'Building sass files for all themes' \
\nfor dir in /app/drupal/web/themes/custom/*; do \
\ncd \"\$dir\"; \
\nyarn install --no-lockfile --link-duplicates\
\ngulp styles \
\ndone \n" > /script.sh

RUN printf "#!/bin/bash \
\n# This is a bash comment inside the script \
\necho 'Monitoring all custom theme files' \
\nfor dir in /app/drupal/web/themes/custom/*; do \
\n  cd \"\$dir\"; \
\n  yarn install --no-lockfile --link-duplicates\
\n  gulp serve & \
\ndone \ 
\nsleep infinity\n" > /watch.sh

RUN chmod +x /script.sh
RUN chmod +x /watch.sh
RUN chmod +x /permissions.sh

RUN npm --global uninstall node-sass
RUN npm --global cache clean -f
RUN yarn global add gulp@^4.0.x gulp-cli gulp-load-plugins browser-sync@^2.24.x del@^3.0.x glob-watcher@^5.0.x

# Create a nonroot user, and switch to it
RUN /usr/sbin/useradd --create-home --home-dir /usr/local/nonroot --shell /bin/bash nonroot
RUN chown -R nonroot /script.sh
RUN chown -R nonroot /watch.sh
RUN npm -g config set user nonroot
RUN chown -R nonroot /usr/local/etc/npmrc

# Switch to our nonroot user
USER nonroot

# Set the HOME var, npm install gets angry if it can't write to the HOME dir,
# which will be /root at this point
ENV HOME /usr/local/nonroot

# ENV NPM_CONFIG_PREFIX=/root/.npmrc
RUN echo 'export PATH="$HOME/.npm/bin:$PATH"' >> ~/.bashrc

WORKDIR /
